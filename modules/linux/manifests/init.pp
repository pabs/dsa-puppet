# common bits for all linux hosts
class linux {
  include ferm
  include ferm::per_host
  include entropykey
  include rng_tools

  file { '/etc/modprobe.d/hwmon.conf':
    content => 'blacklist acpi_power_meter'
  }
}
