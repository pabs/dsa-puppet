# our bacula director
#
# @param pool_name      A token to be used in pool names
# @param db_address     hostname of the postgres server for the catalog DB
# @param db_port        port of the postgres server for the catalog DB
# @param db_name        DB name for the catalog DB
# @param db_user        username for the postgres server for the catalog DB
# @param db_password    password for the postgres server for the catalog DB
# @param port_dir       Port that the director should listen on
# @param db_sslca       SSL CA store for DB access
# @param director_name  bacula name of this dir instance
# @param director_address  address of this dir instance that other instances should connect to (dns name)
# @param messages_name   name of the Messages Resource
class bacula::director(
  String  $db_address = 'localhost',
  Integer $db_port = 5432,
  String  $db_name = 'bacula',
  String  $db_user = 'bacula',
  String  $db_password = hkdf('/etc/puppet/secret', "bacula-db-${::hostname}"),
  String  $pool_name = 'bacula',
  Integer $port_dir = 9101,
  Optional[String] $db_sslca = undef,
  String $director_name = "${::fqdn}-dir",
  Stdlib::Host $director_address = $::fqdn,
  String $messages_name = "Msg-${::fqdn}",
) inherits bacula {

  # used by e.g. bconsole
  $director_secret = hkdf('/etc/puppet/secret', "bacula-dir-${::fqdn}")
  # the RestoreFiles Job needs a Pool and a client.  Any valid pool and client.
  $some_pool_name = "poolfull-${pool_name}-${director_address}"
  $some_client_name = "${director_address}-fd"

  # let the SD know we exist
  @@bacula::storage::director{ $::fqdn:
    tag              => 'bacula::to-storage',
    director_name    => $director_name,
    director_address => $director_address,
    messages_name    => $messages_name,
  }
  # let FDs know we exist
  @@bacula::client::director{ $::fqdn:
    tag              => "bacula::to-fd::${director_address}",
    director_name    => $director_name,
    director_address => $director_address,
    messages_name    => $messages_name,
  }

  ensure_packages ( [
    'bacula-director-pgsql',
    'bacula-common',
    'bacula-common-pgsql'
  ], {
    ensure => 'installed',
  })

  service { 'bacula-director':
    ensure    => running,
    enable    => true,
    hasstatus => true,
    require   => Package['bacula-director-pgsql']
  }
  dsa_systemd::override { 'bacula-director':
    content => @(EOT)
      [Unit]
      After=network-online.target unbound.service
      | EOT
  }

  exec { 'bacula-director reload':
    path        => '/usr/bin:/usr/sbin:/bin:/sbin',
    command     => 'service bacula-director reload',
    refreshonly => true,
  }

  file { '/etc/bacula/conf.d':
    ensure  => directory,
    mode    => '0755',
    group   => bacula,
    purge   => true,
    force   => true,
    recurse => true,
    notify  => Exec['bacula-director reload']
  }

  file { '/etc/bacula/bacula-dir.conf':
    content => template('bacula/bacula-dir.conf.erb'),
    mode    => '0440',
    group   => bacula,
    require => Package['bacula-director-pgsql'],
    notify  => Exec['bacula-director reload']
  }

  file { '/etc/bacula/conf.d/empty.conf':
    content => '',
    mode    => '0440',
    group   => bacula,
    require => Package['bacula-director-pgsql'],
    notify  => Exec['bacula-director reload']
  }

  Bacula::Director::Client_from_storage<<| tag == "bacula::to-director::${::fqdn}" |>>
  Bacula::Director::Client             <<| tag == "bacula::to-director::${::fqdn}" |>>

  package { 'bacula-console':
    ensure => installed;
  }

  file { '/etc/bacula/bconsole.conf':
    content => template('bacula/bconsole.conf.erb'),
    mode    => '0640',
    group   => bacula,
    require => Package['bacula-console']
  }

  package { 'python3-psycopg2': ensure => installed }
  file { '/etc/bacula/scripts/volume-purge-action':
    mode   => '0555',
    source => 'puppet:///modules/bacula/volume-purge-action',
    ;
  }
  file { '/etc/bacula/scripts/volumes-delete-old':
    mode   => '0555',
    source => 'puppet:///modules/bacula/volumes-delete-old',
    ;
  }
  file { '/etc/bacula/storages-list.d':
    ensure  => directory,
    mode    => '0755',
    group   => bacula,
    purge   => true,
    force   => true,
    recurse => true,
  }
  file { '/usr/local/sbin/dsa-bacula-scheduler':
    source => 'puppet:///modules/bacula/dsa-bacula-scheduler',
    mode   => '0555',
  }

  file { '/etc/cron.d/puppet-bacula-stuff': ensure => absent, }
  concat::fragment { 'puppet-crontab--bacula-director':
    target  => '/etc/cron.d/puppet-crontab',
    content => @("EOF"/$)
      @daily root chronic /etc/bacula/scripts/volume-purge-action -v
      @daily root chronic /etc/bacula/scripts/volumes-delete-old -v --token '${pool_name}'
      */3 * * * * root sleep $(( \$RANDOM \\% 60 )); flock -w 0 -e /usr/local/sbin/dsa-bacula-scheduler /usr/local/sbin/dsa-bacula-scheduler
      | EOF
  }

  concat { $bacula::bacula_dsa_client_list:
    ensure_newline => true,
  }
  Concat::Fragment <<| tag == $bacula::tag_bacula_dsa_client_list |>>

  @@ferm::rule::simple { "bacula::director-to-fd::${::fqdn}":
    tag         => "bacula::director-to-fd::${::fqdn}",
    description => 'Allow bacula-fd from the bacula-director',
    port        => '7', # overridden on collecting
    saddr       => $bacula::public_addresses,
  }
  @@ferm::rule::simple { "bacula::director-to-storage::${::fqdn}":
    tag         => 'bacula::director-to-storage',
    description => 'Allow bacula-storage access from the bacula-director',
    chain       => 'bacula-sd',
    saddr       => $bacula::public_addresses,
  }
}
