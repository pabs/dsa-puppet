# Bacula fd config: director snippet
#
# Each/The director exports this class to be collected by each fd.
#
# @param director_name     bacula name of the dir instance
# @param director_address  address of this dir instance that other instances should connect to (dns name)
# @param messages_name   name of the Messages Resource
define bacula::client::director(
  String $director_name,
  Stdlib::Host $director_address,
  String $messages_name,
) {
  include bacula::client

  $dir_client_secret = hkdf('/etc/puppet/secret', "bacula::director<->fd::${director_address}<->${::fqdn}")

  @@bacula::director::client { $bacula::client::client:
    tag            => "bacula::to-director::${director_address}",
    port_fd        => $bacula::client::port_fd,
    client         => $bacula::client::client,
    client_name    => $bacula::client::client_name,
    client_secret  => $dir_client_secret,
    file_retention => $bacula::client::file_retention,
    job_retention  => $bacula::client::job_retention,
  }

  file {
    "/etc/bacula/fd-conf.d/Dir_${director_address}.conf":
      content => template('bacula/client/fd-per-director.conf.erb'),
      mode    => '0440',
      group   => bacula,
      notify  => Exec['bacula-fd restart-when-idle'],
      ;
  }
}
