class roles::packagesqamaster {
  include apache2
  ssl::service { 'packages.qa.debian.org': notify  => Exec['service apache2 reload'], key => true, }

  # Note that there is also role specific config in exim4.conf
  exim::vdomain { 'packages.qa.debian.org':
    owner => 'qa',
    group => 'qa',
  }
}
