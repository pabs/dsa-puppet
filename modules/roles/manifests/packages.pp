class roles::packages {
  class { 'apache2':
    rate_limit => true,
  }

  ssl::service { 'packages.debian.org': notify  => Exec['service apache2 reload'], key => true, }
}
