class rng_tools {
	if $has_dev_hwrng {
		if $::systemproductname == 'ProLiant BL495c G5' {
			$install = false
		} else {
			$install = $::debarchitecture != 's390x' and !($::is_virtual and $::virtual == 'kvm' and (versioncmp($::lsbmajdistrelease, '9') >= 0))
		}
	} else {
		$install = false
	}

	if $install {
		package { 'rng-tools':
			ensure => installed
		}
		service { 'rng-tools':
			ensure  => running,
			require => Package['rng-tools']
		}
	} else {
		package { 'rng-tools':
			ensure => purged
		}
	}
}
