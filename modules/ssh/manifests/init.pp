# @param extraports Addresses/ports to listen on, in addition to 22
class ssh (
  Array[String] $extraports = [],
) {
  package { [ 'openssh-client', 'openssh-server']:
    ensure => installed
  }

  service { 'ssh':
    ensure  => running,
    require => Package['openssh-server']
  }

  ferm::rule::simple { 'dsa-ssh':
    description => 'check ssh access',
    port        => 'ssh',
    target      => 'ssh',
  }
  ferm::rule { 'dsa-ssh-sources':
    description => 'Allow SSH from DSA',
    domain      => '(ip ip6)',
    chain       => 'ssh',
    rule        => 'saddr ($SSH_SOURCES) ACCEPT'
  }
  Ferm::Rule::Simple <<| tag == 'ssh::server::from::nagios' |>>

  file { '/etc/ssh/ssh_config':
    content => template('ssh/ssh_config.erb'),
    require => Package['openssh-client']
  }
  file { '/etc/ssh/sshd_config':
    content => template('ssh/sshd_config.erb'),
    require => Package['openssh-server'],
    notify  => Service['ssh']
  }
  file { '/etc/ssh/userkeys':
    ensure  => directory,
    mode    => '0755',
    require => Package['openssh-server']
  }
  file { '/etc/ssh/puppetkeys':
    ensure  => directory,
    mode    => '0755',
    purge   => true,
    recurse => true,
    force   => true,
    source  => 'puppet:///files/empty/',
    require => Package['openssh-server']
  }
  file { '/etc/ssh/userkeys/root':
    content => template('ssh/authorized_keys.erb'),
  }

  if $facts['systemd'] {
    package { [ 'libpam-systemd' ]:
      ensure => installed
    }
  }
}
