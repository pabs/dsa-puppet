# register an exim manualroute on the mail relays
#
# @param domain     email domain (defaults to $name)
# @param target     targethost
# @param mailport   targetport (defaults to exim::mail_port if set, else 25)
define exim::manualroute (
  String $domain = $name,
  String $target = $::fqdn,
  Optional[Integer] $mailport = undef,
) {
  if $mailport {
    $port = $mailport
  } else {
    $port = lookup( { 'name' => 'exim::mail_port', 'default_value' => 25 } )
  }

  @@concat::fragment { "manualroute-to-${domain}":
    tag     => 'exim::manualroute::to::mailrelay',
    target  => '/etc/exim4/manualroute',
    content => "${$domain}:   ${target}::${port}",
  }
}
