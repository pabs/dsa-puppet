# Install the host's letsencrypt certificate if and only if we have one.
#
# The existance check is using the .key file.
#
define ssl::host_le (
  Enum['present','absent'] $ensure = 'present',
  Stdlib::Host $certname = $fqdn,
  Optional[Stdlib::Absolutepath] $path_crt           = undef, # '/etc/ssl/debian/certs/thishost-le.crt',
  Optional[Stdlib::Absolutepath] $path_crt_chain     = undef, # '/etc/ssl/debian/certs/thishost-le.crt-chain',
  Optional[Stdlib::Absolutepath] $path_crt_chained   = undef, # '/etc/ssl/debian/certs/thishost-le.crt-chained',
  Optional[Stdlib::Absolutepath] $path_key           = undef, # '/etc/ssl/private/thishost-le.key',
  Optional[Stdlib::Absolutepath] $path_key_certchain = undef, #'/etc/ssl/private/thishost-le.key-certchain',
  String $key_group = 'ssl-cert',
  $notify = [],
) {

  $le_dir = hiera('paths.letsencrypt_dir')
  $have_host_le = ($ensure == 'present' and (file("${le_dir}/${certname}.key", '/dev/null') != ''))
  $our_ensure = if $have_host_le { 'present' } else { 'absent' }


  if ($our_ensure == 'absent') {
    if ($path_crt != undef) {
      file { $path_crt:
        ensure => $our_ensure,
        notify => [ Exec['refresh_debian_hashes'], $notify ],
      }
    }
    if ($path_crt_chain != undef) {
      file { $path_crt_chain:
        ensure => $our_ensure,
        notify => [ $notify ],
      }
    }
    if ($path_crt_chained != undef) {
      file { $path_crt_chained:
        ensure => $our_ensure,
        notify => [ $notify ],
      }
    }
    if ($path_key != undef) {
      file { $path_key:
        ensure => $our_ensure,
        notify => [ $notify ],
      }
    }
    if ($path_key_certchain != undef) {
      file { $path_key_certchain:
        ensure => $our_ensure,
        notify => [ $notify ],
      }
    }
  } else {
    if ($path_crt != undef) {
      file { $path_crt:
        ensure  => $our_ensure,
        content => template('ssl/crt.erb'),
        notify  => [ Exec['refresh_debian_hashes'], $notify ],
      }
    }
    if ($path_crt_chain != undef) {
      file { $path_crt_chain:
        ensure  => $our_ensure,
        content => template('ssl/crt-chain.erb'),
        notify  => [ $notify ],
        links   => follow,
      }
    }
    if ($path_crt_chained != undef) {
      file { $path_crt_chained:
        ensure  => $our_ensure,
        content => template('ssl/crt-chained.erb'),
        notify  => [ $notify ],
      }
    }
    if ($path_key != undef) {
      file { $path_key:
        ensure  => $our_ensure,
        mode    => '0440',
        group   => $key_group,
        content => template('ssl/key.erb'),
        notify  => [ $notify ],
        links   => follow,
      }
    }
    if ($path_key_certchain != undef) {
      file { $path_key_certchain:
        ensure  => $our_ensure,
        mode    => '0440',
        group   => $key_group,
        content => template('ssl/key-chained.erb'),
        notify  => [ $notify ],
        links   => follow,
      }
    }
  }
}
