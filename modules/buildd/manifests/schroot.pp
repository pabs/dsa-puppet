# sbuild configuration, including chroots
class buildd::schroot {
  include schroot

    package { 'sbuild':
      ensure => installed,
      tag    => extra_repo,
    }
    package { 'libsbuild-perl':
      ensure => installed,
      tag    => extra_repo,
      before => Package['sbuild']
    }
    file { '/etc/sbuild/sbuild.conf':
      content => template('buildd/sbuild.conf.erb'),
      require => Package['sbuild'],
    }
    if $has_srv_buildd {
      concat::fragment { 'puppet-crontab--buildd-update-schroots':
        target => '/etc/cron.d/puppet-crontab',
        content  => @(EOF)
          13 22 * * 0,3 root PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin setup-all-dchroots buildd
          | EOF
      }
    }
    exec { 'add-buildd-user-to-sbuild':
      command => 'adduser buildd sbuild',
      onlyif  => "getent group sbuild > /dev/null && ! getent group sbuild | grep '\\<buildd\\>' > /dev/null"
    }
}
