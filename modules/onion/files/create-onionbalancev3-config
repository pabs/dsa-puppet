#!/usr/bin/python3

# create onionbalance v3 config file
#
# create an onionbalance config file from a pre-cursor yaml
# file that puppet puts together.


# Copyright (c) 2016, 2021 Peter Palfrader
# Copyright (c) 2021 Silvia Puglisi - hiro@torproject.org
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import os
import os.path
import shutil
import subprocess
import sys
import tempfile
import yaml

j = '/etc/onionbalance/config-dsa-snippetv3.yaml'
conffile = '/etc/onionbalance/config.yaml'

relkeydir = 'private_keys'
keydir = os.path.join('/etc/onionbalance', relkeydir)

with open(j) as conf:
  data = yaml.safe_load(conf)

service_instances = {}
for entry in data:
  s = entry['service']
  if s not in service_instances:
    service_instances[s] = []

  instance = {
    'address': entry['address']+'.onion',
    'name'   : entry['name'],
  }
  service_instances[s].append(instance)

current_keyfiles = {}
service_instance_configs = []
for s in sorted(service_instances):
  keyfile = os.path.join(keydir, s+'.v3key')

  if not os.path.exists(keyfile):
    with tempfile.TemporaryDirectory() as temp_dir:
      subprocess.check_call(['onionbalance-config', '--hs-version', 'v3', '--output', temp_dir, '-n', '1'])

      for fn in os.listdir(temp_dir):
        if not fn.endswith('.key'): continue

        with open( os.path.join(keydir, s + '.info'), 'w') as f:
          print( os.path.splitext(fn)[0], file=f)
          print( "# This file is just for puppet/scripting purposes.  onionbalance does not need it.", file=f)
          print( "# However, this way we get to store the .onion hostname in a readable way.", file=f)

        shutil.move( os.path.join(temp_dir, fn), keyfile )
        shutil.chown(keyfile, group='onionbalance')
        os.chmod(keyfile, 0o640)

        break

    if not os.path.exists(keyfile):
      print("No key for service", s, file=sys.stderr)
      sys.exit(1)
  current_keyfiles[keyfile] = True

  service = {
    'key': keyfile,
    'instances': service_instances[s]
  }
  service_instance_configs.append( service )

with open(conffile, 'w') as f:
  yaml.dump( {'services': service_instance_configs } , f, indent=4)

# remove old keys
for fn in os.listdir(keydir):
  keyfile = os.path.join(keydir, fn)

  if not os.path.isfile(keyfile): continue
  if keyfile.endswith('.v3key'):
    if keyfile not in current_keyfiles:
      os.unlink(keyfile)
  if keyfile.endswith('.info'):
    basekeyfile = os.path.join(keydir, os.path.splitext(fn)[0] + '.v3key')
    if basekeyfile not in current_keyfiles:
      os.unlink(keyfile)
