class tcp_bbr {
  base::linux_module { 'tcp_bbr': }
  base::linux_module { 'sch_fq': }

  base::sysctl { 'puppet-net_core_default_qdisc':
    key   => 'net.core.default_qdisc',
    value => 'fq',
  }
  base::sysctl { 'puppet-net_ipv4_tcp_congestion_control':
    key   => 'net.ipv4.tcp_congestion_control',
    value => 'bbr',
  }
}
