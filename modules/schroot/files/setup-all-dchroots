#!/bin/bash

##
## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
## USE: git clone git+ssh://$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
##

# Copyright (c) 2013 Peter Palfrader
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

get_suites() {
    case "$1" in
      amd64|i386|arm64|armel|armhf)
        echo "sid bookworm bullseye buster stretch"
        ;;
      mips)
        echo "buster"
        ;;
      *)
        echo "sid bookworm bullseye buster"
        ;;
    esac
}

do_one() {
    local a="$1"; shift
    local s="$1"; shift

    case "$MODE" in
        buildd)
            mkdir -p /srv/buildd/unpack
            if ! $SILENT setup-dchroot $EXTRAARGS -f -a "$a" -D -d '/srv/buildd/unpack' -K "$s"
            then
                return 1
            fi
            ;;
        porterbox)
            if ! $SILENT setup-dchroot $EXTRAARGS -f -a "$a" "$s"
            then
                return 1
            fi
            ;;
        *)
            echo >&2 "Invalid mode $MODE"
            exit 1
    esac
    return 0
}

usage()
{
cat << EOF
usage: $0 [<options>] [buildd]

OPTIONS:
    -a <ARCH>  limit to ARCH
    -c         write config only
    -h         this help
    -s <SUITE> limit to SUITE
EOF
}

##########
# "main"
##########


# parse options
##########
EXTRAARGS=""
limitarch=""
limitsuite=""
while getopts "cha:s:" OPTION
do
    case $OPTION in
        a)
            limitarch="$OPTARG"
            ;;
        c)
            EXTRAARGS="$EXTRAARGS -c"
            ;;
        h)
            usage
            exit 0
            ;;
        s)
            limitsuite="$OPTARG"
            ;;
        *)
            usage >&2
            exit 1
            ;;
    esac
done
shift $(($OPTIND - 1))

# parse arguments
##########
if [ "$#" -gt 1 ]; then
    usage >&2
    exit 1
elif [ "$#" = 1 ]; then
    if [ "${1:-}" = "buildd" ]; then
        MODE=buildd
        if ! [ -d /srv/buildd/ ]; then
            echo >&2 "Error: /srv/buildd does not exist or is not a directory."
            exit 1
        fi
    else
        usage >&2
        exit 1
    fi
else
    MODE=porterbox
fi

# figure out whether to be verbose or not
##########
if [ -t 0 ] ; then
    SILENT=""
else
    SILENT="chronic"
fi

# get list of archs based on dpkg architecture
##########
DPKGARCH=$(dpkg --print-architecture)
archs="$DPKGARCH"
case "$DPKGARCH" in
    amd64)
        archs="$archs i386"
        ;;
    arm64)
        archs="$archs armhf armel"
        ;;
    armhf)
        if [ "$(uname -m)" = "aarch64" ] ; then
            archs="$archs arm64"
        fi
        archs="$archs armel"
        ;;
    armel)
        if [ "$(uname -m)" = "armv7l" ] && grep -w vfpv3 -q /proc/cpuinfo ; then
            archs="$archs armhf"
        fi
        ;;
    mips64el)
        archs="$archs mipsel"
        ;;
    mipsel)
        archs="$archs mips64el"
        ;;
esac

err=0

for a in $archs; do
    [ "$limitarch" != "" ] && [ "$limitarch" != "$a" ] && continue
    for s in `get_suites "$a"`; do
        [ "$limitsuite" != "" ] && [ "$limitsuite" != "$s" ] && continue
        if ! do_one "$a" "$s"; then
            err=1
            echo >&2
            echo >&2 "Error: setting up $s:$a dchroot failed."
            echo >&2
            echo >&2
        fi
    done
done

exit $err
