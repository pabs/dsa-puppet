class monitoring::core {
  $vg = "vg_${::hostname}"

  $db_name = 'icinga'
  $db_role = 'icinga'
  $db_password = hkdf('/etc/puppet/secret', "postgresql-${::hostname}-monitoring-${db_role}")

  lvm::logical_volume { 'var-lib-postgres':
    volume_group => $vg,
    size         => '20G',
    fs_type      => 'ext4',
    mountpath    => '/var/lib/postgresql',
  }

  class { 'apache2':
    mpm => 'prefork',
  }
  include apache2::auth_digest
  include apache2::authn_file
  include apache2::module::authn_anon
  include apache2::rewrite
  include apache2::ssl

  class { 'postgresql::globals':
    version => '13',
  }
  class { 'postgresql::server':
  }


  ensure_packages( [
    'icinga2',
    'icingaweb2',
    'icingacli',
    'icingaweb2-module-monitoring',
    'nagios-images',
    'php-curl',
    'php-gd',
    'php-ldap',
    'php-intl',
    'php-imagick',
    'php-pgsql',
    'dbconfig-no-thanks',
    'icinga2-ido-pgsql',
  ])


  postgresql::server::pg_hba_rule { 'local-md5-icinga':
    description => 'Accept md5 auth on local connections for icinga',
    type        => 'local',
    database    => $db_name,
    user        => $db_role,
    auth_method => 'md5',
    order       => 1,
  }

  postgresql::server::db { $db_name:
    user     => $db_role,
    password => postgresql_password($db_role, $db_password),
    encoding => 'UTF8',
    notify   => Exec['icinga-init-db'],
  }

  exec { 'icinga-init-db':
    command     => 'psql -t -U icinga icinga < /usr/share/icinga2-ido-pgsql/schema/pgsql.sql',
    refreshonly => true,
    environment => [ "PGPASSWORD=${db_password}" ],
    onlyif      => "psql -t -U icinga icinga -c \"SELECT count(*) > 0 from information_schema.tables where table_schema = 'public'\" | grep -q 'f'",
  }

  file { '/etc/icinga2/features-enabled/ido-pgsql.conf':
    ensure => link,
    target => '../features-available/ido-pgsql.conf',
    notify => Exec['systemctl restart icinga2'],
  }
  file { '/etc/icinga2/features-available/ido-pgsql.conf':
    content => @("EOF"),
      library "db_ido_pgsql"
      object IdoPgsqlConnection "ido-pgsql" {
        user = "${db_role}",
        password = "${db_password}",
        host = "localhost",
        database = "${db_name}"
      }
      | EOF
    mode    => '0640',
    owner   => 'root',
    group   => 'nagios',
    notify  => Exec['systemctl restart icinga2'],
  }
  exec{ 'systemctl restart icinga2':
    refreshonly => true,
  }


  file { '/etc/apache2/conf-enabled/icingaweb2.conf':
    ensure => absent,
    notify => Exec['service apache2 reload'],
  }
  file { '/etc/apache2/mon-htpasswd':
    owner => 'root',
    group => 'www-data',
    mode  => '0640',
  }

  ssl::service { 'mon.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  onion::service { 'lintian.debian.org': port => 80, target_address => 'mon.debian.org', target_port => 80, direct => true }
  apache2::site { 'mon.debian.org':
    content => template('monitoring/apache-mon.debian.org.conf.erb')
  }

  # icingaweb2 config:
  file { '/etc/icingaweb2/authentication.ini':
    content => @(EOF),
      [autologin]
      backend = external
      | EOF
  }
}
