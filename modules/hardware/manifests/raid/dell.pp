class hardware::raid::dell {
	if "$::manufacturer" == "Dell Inc." {
		include debian_org::apt_restricted

		package { 'srvadmin-server-cli':
			ensure  => installed,
			tag    => extra_repo,
		}
		package { 'srvadmin-storage-cli':
			ensure  => installed,
			tag    => extra_repo,
		}
		package { 'srvadmin-omcommon':
			ensure  => installed,
			tag    => extra_repo,
		}
		package { 'libssl1.0.0':
			ensure  => installed,
			tag    => extra_repo,
		}
		package { 'libxslt1.1':
			ensure => installed,
		}
	}

	# buggy and conflicts with dell's own thing
	dsa_systemd::mask { 'openipmi.service': }
}
