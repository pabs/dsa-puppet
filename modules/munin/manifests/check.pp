# enable (or disable) a munin check
# @param ensure check enabled/disabled
# @param script check to synclink
define munin::check(
  Enum['present','absent'] $ensure = 'present',
  String $script = $name
) {
  include munin

  $link_target = $ensure ? {
    present => link,
    absent  => absent,
  }

  file { "/etc/munin/plugins/${name}":
    ensure  => $link_target,
    target  => "/usr/share/munin/plugins/${script}",
    require => Package['munin-node'],
    notify  => Service['munin-node'],
  }
}
