#!/usr/bin/python3

# simple script to write a interfaces file from ganeti metadata
#
# this relies on variables that are documented in:
#
# http://docs.ganeti.org/ganeti/master/html/man-ganeti-os-interface.html
#
# specifically, for example:
#
# NIC_COUNT=1
# NIC_0_IP=192.0.2.1
# NIC_0_NETWORK_SUBNET=192.0.2.0/24
# NIC_0_NETWORK_GATEWAY=192.0.2.1
# TARGET=./foo


import argparse
import ipaddress
import logging
import logging.handlers
import os
import pathlib
import re
import subprocess
import sys
import tempfile


# should match a line that sources /etc/network/interfaces.d
SOURCE_REGEX = r'^source(-directory)?\s+/etc/network/interfaces.d(/\*)?$'
# the line that should be present if the above doesn't match
SOURCE_STRING = b"\nsource /etc/network/interfaces.d/*\n"
# header to add to generated files
HEADER = "# automatically configured interface\nauto eth%d\n"


def ensure_interface_d(target_path, interfaces='etc/network/interfaces'):
    '''ensure interfaces.d is included in /etc/network/interfaces and that
    directory structure exists'''
    # this will create /etc/network if missing as well, in the weird
    # cases where we're not really in Debian
    (target_path / 'etc' / 'network' / 'interfaces.d').mkdir(parents=True,
                                                             exist_ok=True)
    interfaces_path = target_path / interfaces
    with interfaces_path.open() as fp:
        content = fp.read()

    if not re.search(SOURCE_REGEX, content, re.M):
        with interfaces_path.open(mode='ab') as fp:
            fp.write(SOURCE_STRING)


def ensure_resolv_conf(target_path, resolvconf, nameservers):
    '''add given resolvers to resolv.conf'''
    with (target_path / resolvconf).open(mode='a') as fp:
        fp.write("\n# 'safe' defaults until local resolvers are configured\n")
        for nameserver in nameservers:
            fp.write("nameserver %s\n" % nameserver)


def ipv4_section(i):
    '''generate an IPv4 section'''

    address = gnt_get_nic(i, 'IP')
    if address is None:
        logging.warning('missing IPv4 address for interface #%d, skipping', i)
        return ''
    ip = ipaddress.IPv4Network(gnt_get_nic(i, 'NETWORK_SUBNET',
                                           '0.0.0.0/32'))
    gateway = gnt_get_nic(i, 'NETWORK_GATEWAY')
    content = '''iface eth%d inet static
    address %s/%s
''' % (i, address, ip.prefixlen)
    if gateway:
        content += "    gateway %s\n" % gateway
    content += "\n"
    logging.info('configured eth%d with IPv4 address: %s/%s%s',
                 i, address, ip.prefixlen,
                 ' (gateway: %s)' % gateway if gateway else '')
    return content


def ipv6_section(i):
    '''generate a section for the IPv6 subnet, if available'''
    content = ''
    subnet6 = gnt_get_nic(i, 'NETWORK_SUBNET6')
    mac = gnt_get_nic(i, 'MAC')
    if subnet6 and mac:
        command = ['ipv6calc', '--action', 'prefixmac2ipv6',
                   '--in', 'prefix+mac', '--out', 'ipv6',
                   subnet6, mac]
        logging.debug('manual SLAAC allocation with: %s',
                      ' '.join(command))
        try:
            address6 = subprocess.check_output(command).decode('utf-8').strip()
        except FileNotFoundError as e:
            logging.error('cannot find IPv6 address, install ipv6calc: %s',
                          e)
        content += '''iface eth%d inet6 static
    address %s
''' % (i, address6)
        gateway6 = gnt_get_nic(i, 'NETWORK_GATEWAY6')
        if gateway6:
            content += "    gateway %s\n" % gateway6
        content += "    accept_ra 0\n"
        content += "\n"
        logging.info('configured eth%d with IPv6 address: %s%s',
                     i, address6,
                     ' (gateway: %s)' % gateway6 if gateway6 else '')
    else:
        state = []
        if not mac:
            state.append('NIC_%d_MAC' % i)
        if not subnet6:
            state.append('NIC_%d_NETWORK_SUBNET6' % i)
        logging.warning('no IPv6 defined on interface #%d: %s missing',
                        i, ' and '.join(state))
    return content


def gnt_get_nic(i, suffix, default=None):
    '''extract specific environment variable from Ganeti'''
    return os.environ.get('NIC_%d_%s' % (i, suffix), default)


def write_interfaces_files(target):
    '''actually write the (possible multiple) interfaces.d/ethN files'''
    count = int(os.environ.get('NIC_COUNT', 1))
    if 'NIC_%d_IP' % (count-1) not in os.environ:
        logging.warning('missing interface %s, bad NIC_COUNT: %d',
                        'NIC_%d_IP' % (count-1),
                        count)
        # this should be a failure, but we still get NIC_COUNT=1 and
        # NIC_0_IP if no --ip is provided
        return 0
    logging.info('Configuring %d interfaces', count)
    for i in range(count):
        logging.info('Configuring interface #%d', i)
        content = ipv6_section(i)
        content += ipv4_section(i)

        if not content:
            logging.warning('no config found for interface #%d, skipped', i)
            continue
        filename = pathlib.Path(target) / 'etc' / 'network' / 'interfaces.d' / ('eth%d' % i)  # noqa: E501
        try:
            with filename.open(mode='w') as interfaces:
                interfaces.write(HEADER % i + content)
        except IOError:
            logging.error("Cannot write to '%s', aborting", filename)
    return 0


def main():
    target = os.environ.get('TARGET')
    if (not target or target == '/'):
        logging.error("Invalid target directory '%s', aborting", target)
        return 1

    target_path = pathlib.Path(target)
    ensure_interface_d(target_path)
    ensure_resolv_conf(target_path,
                       pathlib.Path('etc') / 'resolv.conf',
                       ['8.8.8.8', '2001:4860:4860::8888'])
    status = write_interfaces_files(os.environ.get('TARGET'))
    return status


def test_reset_environment():
    '''helper function to reset the test environment'''
    try:
        del os.environ['NIC_COUNT']
        del os.environ['NIC_0_IP']
        del os.environ['NIC_0_NETWORK_SUBNET6']
        del os.environ['NIC_0_NETWORK_GATEWAY6']
        del os.environ['NIC_0_NETWORK_SUBNET']
        del os.environ['NIC_0_NETWORK_GATEWAY']
        del os.environ['NIC_0_MAC']
    except KeyError:
        pass


def test_ensure_interface_d():
    '''test stub for ensure_interface_d()'''
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpdir = pathlib.Path(tmpdir)
        content = b"#source foo\n#source /etc/network/interfaces.d\n"
        # need to create at least /etc/network here
        tmpfile = tmpdir / 'etc' / 'network'
        tmpfile.mkdir(parents=True, exist_ok=False)
        tmpfile = tmpfile / 'interfaces'
        with tmpfile.open(mode='ab+') as fp:
            fp.write(content)
            fp.flush()
            ensure_interface_d(tmpdir)
            assert (tmpdir / 'etc' / 'network' / 'interfaces.d').exists()
            fp.seek(0)
            assert content + SOURCE_STRING == fp.read(), "line added"
            ensure_interface_d(tmpdir)
            fp.flush()
            fp.seek(0)
            assert content + SOURCE_STRING == fp.read(), "added only once"


def test_ipv4():
    '''test stub for ipv4_section()'''
    test_reset_environment()
    os.environ['NIC_COUNT'] = '1'
    os.environ['NIC_0_IP'] = '192.0.2.2'

    assert ipv4_section(0) == '''iface eth0 inet static
    address 192.0.2.2/32

''', 'files are different'

    os.environ['NIC_0_IP'] = '192.0.2.2'
    os.environ['NIC_0_NETWORK_SUBNET'] = '192.0.2.0/24'
    os.environ['NIC_0_NETWORK_GATEWAY'] = '192.0.2.1'

    assert ipv4_section(0) == '''iface eth0 inet static
    address 192.0.2.2/24
    gateway 192.0.2.1

''', 'files are different'


def test_ipv6():
    '''test stub for ipv6_section()'''
    test_reset_environment()
    os.environ['NIC_COUNT'] = '1'
    os.environ['NIC_0_NETWORK_SUBNET6'] = '2001:2::/48'
    os.environ['NIC_0_NETWORK_GATEWAY6'] = '2001:2::1'
    os.environ['NIC_0_MAC'] = '01:23:45:67:89:AB'
    assert ipv6_section(0) == '''iface eth0 inet6 static
    address 2001:2::323:45ff:fe67:89ab/48
    gateway 2001:2::1
    accept_ra 0

''', 'files are different'


def test_main():
    '''test stub for main()'''
    test_reset_environment()
    memory_handler = logging.handlers.BufferingHandler(2)
    # Add this handler to the monkeysign.ui logger
    logger = logging.getLogger('')
    logger.addHandler(memory_handler)

    assert main() == 1, 'missing target is expected'
    assert 'Invalid target directory' in memory_handler.buffer[0].getMessage()
    memory_handler.flush()
    with tempfile.TemporaryDirectory() as tmpdirname:
        os.environ['TARGET'] = tmpdirname
        assert write_interfaces_files(tmpdirname) == 0, 'noop expected'
        assert 'missing interface' in memory_handler.buffer[0].getMessage()
        memory_handler.flush()

        networkdir = pathlib.Path(tmpdirname) / 'etc' / 'network' / 'interfaces.d'  # noqa: E501
        networkdir.mkdir(parents=True)
        os.environ['NIC_COUNT'] = '1'
        assert write_interfaces_files(tmpdirname) == 0
        assert 'missing interface' in memory_handler.buffer[0].getMessage()  # noqa: E501
        memory_handler.flush()

        # full stack test: two interfaces, with IPv6 for the first
        os.environ['NIC_COUNT'] = '2'
        os.environ['NIC_0_IP'] = '192.0.2.2'
        os.environ['NIC_0_NETWORK_SUBNET'] = '192.0.2.0/24'
        os.environ['NIC_0_NETWORK_GATEWAY'] = '192.0.2.1'
        os.environ['NIC_0_NETWORK_SUBNET6'] = '2001:2::/48'
        os.environ['NIC_0_NETWORK_GATEWAY6'] = '2001:2::1'
        os.environ['NIC_0_MAC'] = '01:23:45:67:89:AB'
        os.environ['NIC_1_IP'] = '198.51.100.2'
        status = write_interfaces_files(tmpdirname)
        interfaces = networkdir / 'eth0'
        with interfaces.open() as f:
            assert f.read() == '''# automatically configured interface
auto eth0
iface eth0 inet6 static
    address 2001:2::323:45ff:fe67:89ab/48
    gateway 2001:2::1
    accept_ra 0

iface eth0 inet static
    address 192.0.2.2/24
    gateway 192.0.2.1

''', 'files are different'
        assert status == 0, 'should have returned with zero'

        interfaces2 = networkdir / 'eth1'
        assert interfaces2.exists()
        with interfaces2.open() as f:
            assert f.read() == '''# automatically configured interface
auto eth1
iface eth1 inet static
    address 198.51.100.2/32

'''


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--test', '-t', action='store_true',
                        help='run test suite, and nothing else')
    args = parser.parse_args()
    # should depend on DEBUG_LEVEL
    logging.basicConfig(format='%(message)s', level=logging.INFO)

    if args.test:
        test_main()
        test_ipv4()
        test_ipv6()
        test_ensure_interface_d()
        sys.exit(0)
    sys.exit(main())
